package mx.com.cardif.carga.catalogos;

import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import mx.com.cardif.carga.catalogos.commons.SpringConfigConstant;

@Component
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = SpringConfigConstant.DATASOURCE_CONFIG_ENTITY_MANAGER, transactionManagerRef = SpringConfigConstant.DATASOURCE_CONFIG_TRANSACTION_MANAGER, basePackages = {
		SpringConfigConstant.DATASOURCE_CONFIG_PACKAGE_SCAN })
public class DataSourceTestConfig {
	
	@Value("${spring.datasource.driverClassName}")
	private String driverClass;
	
	@Value("${spring.datasource.url}")
	private String url;
	
	@Value("${spring.datasource.username}")
	private String user;
	
	@Value("${spring.datasource.password}")
	private String pass;

	@Bean(name = SpringConfigConstant.DATASOURCE_CONFIG_ENTITY_MANAGER)
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(getDataSource());
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalJpaProperties());
		em.setPersistenceUnitName(SpringConfigConstant.DATASOURCE_CONFIG_PERSISTENCE_UNIT);
		em.setPackagesToScan(SpringConfigConstant.DATASOURCE_CONFIG_PACKAGE_SCAN);
		return em;
	}

	@Bean(name = SpringConfigConstant.DATASOURCE_CONFIG_TRANSACTION_MANAGER)
	@Primary
	public PlatformTransactionManager transactionManager() throws NamingException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setDataSource(getDataSource());
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

		return transactionManager;
	}

	@Bean(name = "catalogosDataSource")
	@Primary
	public DataSource getDataSource() {
		DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName(driverClass);
		dataSourceBuilder.url(url);
		dataSourceBuilder.username(user);
		dataSourceBuilder.password(pass);
		return dataSourceBuilder.build();
	}

	Properties additionalJpaProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "validate");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
		properties.setProperty("hibernate.id.new_generator_mappings", "true");
		return properties;
	}

}
