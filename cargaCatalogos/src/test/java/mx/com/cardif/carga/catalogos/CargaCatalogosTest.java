package mx.com.cardif.carga.catalogos;

import java.util.Date;
import java.util.UUID;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.MetaDataInstanceFactory;
import org.springframework.batch.test.StepScopeTestExecutionListener;
import org.springframework.batch.test.StepScopeTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cardif.carga.catalogos.commons.CatalogosDTO;
import mx.com.cardif.carga.catalogos.commons.SpringConfigConstant;
import mx.com.cardif.carga.catalogos.config.BatchConfig;
import mx.com.cardif.carga.catalogos.dao.PsnBitacoraCargaCatalogos;
import mx.com.cardif.carga.catalogos.util.DBWritterBitacoraCargaCatalogos;
import mx.com.cardif.carga.catalogos.util.DBWritterCatalogos;
import mx.com.cardif.carga.catalogos.util.ProcessorBitacoraCargaCatalogos;
import mx.com.cardif.carga.catalogos.util.ProcessorCatalogos;

@RunWith(SpringRunner.class)
@SpringBatchTest
@SpringBootTest(classes = { BatchConfig.class, DataSourceTestConfig.class, ProcessorCatalogos.class,
		DBWritterCatalogos.class, ProcessorBitacoraCargaCatalogos.class, DBWritterBitacoraCargaCatalogos.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		StepScopeTestExecutionListener.class })
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
@Profile("test")
class CargaCatalogosTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(CargaCatalogosTest.class);

	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;

	@Autowired
	private JobRepositoryTestUtils jobRepositoryTestUtils;

	@Autowired
	private ProcessorBitacoraCargaCatalogos processor;

	@Autowired
	private ProcessorCatalogos processorCatalogos;

	private FlatFileItemReader<String> itemReader = new FlatFileItemReader<String>();
	private static final String INPUT_FILE_CALLES = "src/test/resources/ins_catcalles_db2_20200208.sql";
	private static final String INPUT_FILE_ZONAS = "src/test/resources/ins_catzonas_db2_20200208.sql";
	private static final String INPUT_FILE_INCORRECTO = "src/test/resources/archivoIncorrecto.sql";

	@BeforeEach
	public void init() throws Exception {

	}

	private JobParameters defaultJobParameters() {
		JobParametersBuilder paramsBuilder = new JobParametersBuilder();

		paramsBuilder.addString("input_file_name", INPUT_FILE_CALLES);
		paramsBuilder.addString("file_name", "ins_catcalles_db2_20200208.sql");
		paramsBuilder.addDate("fechaEjecucion", new Date());
		paramsBuilder.addString("idJob", UUID.randomUUID().toString());

		return paramsBuilder.toJobParameters();
	}

	@Test
	void cargaCatalogosJob() throws Exception {
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(defaultJobParameters());
		JobInstance actualJobInstance = jobExecution.getJobInstance();
		Assert.assertEquals(SpringConfigConstant.BATCH_CONFIG_JOB_CARGA_CATALOGOS, actualJobInstance.getJobName());
		Assert.assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
	}

	@Test
	@Rollback
	@Transactional
	void guardaBitacoraTest() throws Exception {
		JobExecution stepExecution = jobLauncherTestUtils
				.launchStep(SpringConfigConstant.BATCH_CONFIG_STEP_GUARDA_BITACORA, defaultJobParameters());
		Assert.assertEquals(BatchStatus.COMPLETED, stepExecution.getStatus());
	}

	public StepExecution getStepExecution() {
		return MetaDataInstanceFactory.createStepExecution(defaultJobParameters());
	}

	@Test
	void processorBitacoraTest() throws Exception {
		String archivo = "INSERT INTO DB_BCPL.catcalles values(901480,'2 CDA SAN MIGUEL XICALCO');";
		final PsnBitacoraCargaCatalogos bitacora = StepScopeTestUtils.doInStepScope(getStepExecution(),
				() -> processor.process(archivo));
		Assert.assertNotNull(bitacora);
	}
	
	@Test
	void processorCatalogoTest() throws Exception {
		String archivo = "INSERT INTO DB_BCPL.catcalles values(901480,2 'CDA SAN MIGUEL XICALCO');";
		final CatalogosDTO bitacora = StepScopeTestUtils.doInStepScope(getStepExecution(),
				() -> processorCatalogos.process(archivo));
		Assert.assertNotNull(bitacora);
	}
	
	@Test
	void processorCatalogoCadenaIncorrectaTest() throws Exception {
		String archivo = "INSERT INTO DB_BCPL.catcalles";
		final CatalogosDTO bitacora = StepScopeTestUtils.doInStepScope(getStepExecution(),
				() -> processorCatalogos.process(archivo));
		Assert.assertNull(bitacora);
	}
	
	@Test
	void processorCatalogoValoresIncorrectosTest() throws Exception {
		String archivo = "INSERT INTO DB_BCPL.catcalles values(901480, CDA SAN MIGUEL XICALCO);";
		final CatalogosDTO bitacora = StepScopeTestUtils.doInStepScope(getStepExecution(),
				() -> processorCatalogos.process(archivo));
		Assert.assertNotNull(bitacora);
	}
	
	@Test
	void processorCatalogoZonasValoresIncorrectosTest() throws Exception {
		String archivo = "INSERT INTO DB_BCPL.catzonas values(0,31623,'SANTA CRUZ POCITOS','ATLTZAYANCA','ATLTZAYANCA',90558,'','',0,0,0,0,0,'',0,0,0,2,'',501402)";
		final CatalogosDTO bitacora = StepScopeTestUtils.doInStepScope(getStepExecution(),
				() -> processorCatalogos.process(archivo));
		Assert.assertNotNull(bitacora);
	}
	
	@Test
	void processorCatalogoValoresIncompletosTest() throws Exception {
		String archivo = "INSERT INTO DB_BCPL.catcalles values(CDA SAN MIGUEL XICALCO);";
		final CatalogosDTO bitacora = StepScopeTestUtils.doInStepScope(getStepExecution(),
				() -> processorCatalogos.process(archivo));
		Assert.assertNull(bitacora.getCalle());
	}
	
	@Test
	void processorCatalogoIncorrectoTest() throws Exception {
		String archivo = "INSERT INTO DB_BCPL.catMunicipios values(901480,'2 CDA SAN MIGUEL XICALCO');";
		final CatalogosDTO bitacora = StepScopeTestUtils.doInStepScope(getStepExecution(),
				() -> processorCatalogos.process(archivo));
		Assert.assertNull(bitacora);
	}

	@Test
	void itemReaderTest() throws Exception {
		String EXPECTED_CATALOGO = "catcalles";
		itemReader.setLineMapper(new PassThroughLineMapper());
		itemReader.setResource(new FileSystemResource(INPUT_FILE_CALLES));
		itemReader.open(MetaDataInstanceFactory.createStepExecution().getExecutionContext());
		try {
			String line;
			while ((line = itemReader.read()) != null) {
				MatcherAssert.assertThat(line, CoreMatchers.containsString(EXPECTED_CATALOGO));
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			itemReader.close();
		}
	}

	@Test
	void itemReaderArchivoIncorrectoTest() throws Exception {
		String EXPECTED_CATALOGO = "catcalles";
		itemReader.setLineMapper(new PassThroughLineMapper());
		itemReader.setResource(new FileSystemResource(INPUT_FILE_INCORRECTO));
		itemReader.open(MetaDataInstanceFactory.createStepExecution().getExecutionContext());
		try {
			String line;
			while ((line = itemReader.read()) != null) {
				Assert.assertNotEquals(line, EXPECTED_CATALOGO);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			itemReader.close();
		}
	}

	@Test
	void itemReaderZonasTest() throws Exception {
		String EXPECTED_CATALOGO = "catzonas";
		itemReader.setLineMapper(new PassThroughLineMapper());
		itemReader.setResource(new FileSystemResource(INPUT_FILE_ZONAS));
		itemReader.open(MetaDataInstanceFactory.createStepExecution().getExecutionContext());
		try {
			String line;
			while ((line = itemReader.read()) != null) {
				MatcherAssert.assertThat(line, CoreMatchers.containsString(EXPECTED_CATALOGO));
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			itemReader.close();
		}
	}

	@Test
	void cargaCatalogosZonasJob() throws Exception {
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(defaultJobParametersZonas());
		JobInstance actualJobInstance = jobExecution.getJobInstance();
		Assert.assertEquals(SpringConfigConstant.BATCH_CONFIG_JOB_CARGA_CATALOGOS, actualJobInstance.getJobName());
		Assert.assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
	}

	@Test
	@Rollback
	@Transactional
	void guardaBitacoraZonasTest() throws Exception {
		JobExecution jobExecution = jobLauncherTestUtils
				.launchStep(SpringConfigConstant.BATCH_CONFIG_STEP_GUARDA_BITACORA, defaultJobParametersZonas());
		Assert.assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
	}

	private JobParameters defaultJobParametersZonas() {
		JobParametersBuilder paramsBuilder = new JobParametersBuilder();
		paramsBuilder.addString("input_file_name", INPUT_FILE_ZONAS);
		paramsBuilder.addString("file_name", "ins_catzonas_db2_20200208.sql");
		paramsBuilder.addDate("fechaEjecucion", new Date());
		paramsBuilder.addString("idJob", UUID.randomUUID().toString());
		return paramsBuilder.toJobParameters();
	}

	@After
	public void finalize() {
		LOGGER.info("********************* finalize ****************************");
		jobRepositoryTestUtils.removeJobExecutions();
	}

}
