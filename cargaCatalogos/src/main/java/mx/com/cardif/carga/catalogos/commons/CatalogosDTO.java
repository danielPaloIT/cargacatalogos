package mx.com.cardif.carga.catalogos.commons;

import mx.com.cardif.carga.catalogos.dao.PsnCatCalles;
import mx.com.cardif.carga.catalogos.dao.PsnCatZonas;

public class CatalogosDTO {

	private PsnCatCalles calle;
	private PsnCatZonas zona;

	public PsnCatCalles getCalle() {
		return calle;
	}

	public void setCalle(PsnCatCalles calle) {
		this.calle = calle;
	}

	public PsnCatZonas getZona() {
		return zona;
	}

	public void setZona(PsnCatZonas zona) {
		this.zona = zona;
	}

	@Override
	public String toString() {
		return "CatalogosDTO [calle=" + calle + ", zona=" + zona + "]";
	}

}
