package mx.com.cardif.carga.catalogos.dao;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PSN_BITACORA_CARGA_CATALOGOS")
public class PsnBitacoraCargaCatalogos {

	@Id
	@Column(name = "ID_ARCHIVO", columnDefinition = "uniqueidentifier")
	private String idArchivo;

	@Column(name = "NOMBRE_ARCHIVO", columnDefinition = "nvarchar")
	private String nombreArchivo;

	@Column(name = "RUTA_ARCHIVO", columnDefinition = "nvarchar")
	private String rutaArchivo;

	@Column(name = "FECHA_CARGA")
	private Date fechaCarga;

	public PsnBitacoraCargaCatalogos() {
		// default implementation ignored
	}

	public String getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(String idArchivo) {
		this.idArchivo = idArchivo;
	}

	public UUID getUUID() {
		return UUID.fromString(idArchivo);
	}

	public void setUUID(UUID val) {
		idArchivo = val.toString();
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	@Override
	public String toString() {
		return "PsnBitacoraCargaCatalogos [idArchivo=" + idArchivo + ", nombreArchivo=" + nombreArchivo
				+ ", rutaArchivo=" + rutaArchivo + ", fechaCarga=" + fechaCarga + "]";
	}

}
