package mx.com.cardif.carga.catalogos.util;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import mx.com.cardif.carga.catalogos.dao.PsnBitacoraCargaCatalogos;

@Component
@StepScope
public class ProcessorBitacoraCargaCatalogos implements ItemProcessor<String, PsnBitacoraCargaCatalogos> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessorBitacoraCargaCatalogos.class);

	@Value("#{jobParameters[file_name]}")
	private String nombre;

	@Value("#{jobParameters[input_file_name]}")
	private String ruta;

	@Value("#{jobParameters[idJob]}")
	private UUID idJob;

	@Override
	public PsnBitacoraCargaCatalogos process(String archivo) throws Exception {
		Date fechaCarga = new Date();
		LOGGER.info("[Start][processorBitacora]input:[valor={ '{}' }]", archivo);
		PsnBitacoraCargaCatalogos bitacora = new PsnBitacoraCargaCatalogos();
		bitacora.setUUID(idJob);
		bitacora.setNombreArchivo(nombre);
		bitacora.setRutaArchivo(ruta);
		bitacora.setFechaCarga(fechaCarga);
		LOGGER.info("[End][processorBitacora]Output:[valor={ '{}' }]", bitacora);
		return bitacora;
	}

}
