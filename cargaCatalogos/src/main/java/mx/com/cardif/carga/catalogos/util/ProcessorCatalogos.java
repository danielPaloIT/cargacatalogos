package mx.com.cardif.carga.catalogos.util;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import mx.com.cardif.carga.catalogos.commons.CatalogosDTO;
import mx.com.cardif.carga.catalogos.commons.EsquemasBDConstant;
import mx.com.cardif.carga.catalogos.dao.PsnCatCalles;
import mx.com.cardif.carga.catalogos.dao.PsnCatZonas;

@Component
@StepScope
public class ProcessorCatalogos implements ItemProcessor<String, CatalogosDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessorCatalogos.class);

	@Value("#{jobParameters[file_name]}")
	private String nombre;

	@Value("#{jobParameters[idJob]}")
	private UUID idJob;

	@Value("#{jobParameters[fechaEjecucion]}")
	private Date fechaEjecucion;

	@Override
	public CatalogosDTO process(String archivo) {
		LOGGER.info("[Start][processorCatalogos]input:[valor={ '{}' }]", archivo);
		try {
			CatalogosDTO catalogos = new CatalogosDTO();
			String[] valores = archivo.substring(archivo.indexOf('(') + 1, archivo.indexOf(')')).split(",");
			if (archivo.contains(EsquemasBDConstant.CATALOGO_CALLES)) {
				catalogos.setCalle(obtieneCalle(valores));
			} else if (archivo.contains(EsquemasBDConstant.CATALOGO_ZONAS)) {
				catalogos.setZona(obtieneZona(valores));
			} else {
				LOGGER.debug("[processorCatalogos]Output:[valor={ No se encontro un eschema valido }]");
				return null;
			}
			LOGGER.info("[End][processorCatalogos]Output:[valor={ '{}' }]", catalogos);
			return catalogos;
		} catch (StringIndexOutOfBoundsException e) {
			LOGGER.error("[Error][processorCatalogos] {}]", e.getMessage());
			return null;
		}
	}

	private PsnCatCalles obtieneCalle(String[] valores) {
		if (valores.length >= 2) {
			PsnCatCalles calle = new PsnCatCalles();
			calle.setNumeroCalle(Integer.valueOf(valores[0]));
			calle.setNombreCalle(limipaComillas(valores[1]));
			calle.setIdBitacora(idJob.toString());
			calle.setFechaCreacion(fechaEjecucion);
			return calle;
		}else {
			LOGGER.error("[Error][processorCatalogos] {Faltan valores en el catalogo de calles}]");
			return null;
		}

	}

	private PsnCatZonas obtieneZona(String[] valores) {
		if (valores.length >= 23) {
			PsnCatZonas zona = new PsnCatZonas();
			zona.setNumeroCiudad(Integer.valueOf(valores[0]));
			zona.setNumeroColonia(Integer.valueOf(valores[1]));
			zona.setNombrezona(limipaComillas(valores[2]));
			zona.setPoblacionZona(limipaComillas(valores[3]));
			zona.setMunicipioZona(limipaComillas(valores[4]));
			zona.setCodigoPostalZona(Integer.valueOf(valores[5]));
			zona.setPlanoZona(limipaComillas(valores[6]));
			zona.setRumboZona(limipaComillas(valores[7]));
			zona.setSupervisorZona(Integer.valueOf(valores[8]));
			zona.setChoferZona(Integer.valueOf(valores[9]));
			zona.setJefeGrupoZona(Integer.valueOf(valores[10]));
			zona.setGerenteZona(Integer.valueOf(valores[11]));
			zona.setAbogadoZona(Integer.valueOf(valores[12]));
			zona.setMarcaEncuestaDias(limipaComillas(valores[13]));
			zona.setNumeroCalle(Integer.valueOf(valores[14]));
			zona.setNumeroCasa(Integer.valueOf(valores[15]));
			zona.setMarcaUnidadHabitacional(limipaComillas(valores[16]));
			zona.setNumeroDivisionCobranzas(Integer.valueOf(valores[17]));
			zona.setClaveAbogado(Integer.valueOf(valores[18]));
			zona.setCiudadCobranzas(Integer.valueOf(valores[19]));
			zona.setNumeroCobranzas(Integer.valueOf(valores[20]));
			zona.setClaveAragon(limipaComillas(valores[21]));
			zona.setCentro(Integer.valueOf(valores[22]));
			zona.setIdBitacora(idJob.toString());
			zona.setFechaCreacion(fechaEjecucion);
			return zona;
		}else {
			LOGGER.error("[Error][processorCatalogos] {Faltan valores en el catalogo de zonas}]");
			return null;
		}
	}

	private String limipaComillas(String campo) {
		campo = campo.substring(campo.indexOf('\'') + 1, campo.length() - 1);
		return campo;
	}

}
