package mx.com.cardif.carga.catalogos.commons;

public class EsquemasBDConstant {

	private EsquemasBDConstant() {

	}

	public static final String CATALOGO_CALLES = "catcalles";
	public static final String CATALOGO_ZONAS = "catzonas";
	public static final String ERROR_SCHEMA_CATALOGOS = "El archivo no contiene informacion valida para ejecutarse en base de datos";

}
