package mx.com.cardif.carga.catalogos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CargaCatalogosApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
		return app.sources(CargaCatalogosApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(CargaCatalogosApplication.class, args);
	}

}
