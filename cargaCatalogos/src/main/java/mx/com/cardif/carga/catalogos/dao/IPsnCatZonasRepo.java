package mx.com.cardif.carga.catalogos.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPsnCatZonasRepo extends JpaRepository<PsnCatZonas, Integer> {

}
