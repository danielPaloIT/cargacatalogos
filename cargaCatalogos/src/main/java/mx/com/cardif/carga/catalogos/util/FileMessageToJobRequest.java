package mx.com.cardif.carga.catalogos.util;

import java.io.File;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.integration.launch.JobLaunchRequest;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;

public class FileMessageToJobRequest {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileMessageToJobRequest.class);

	private Job job;
	private String fileParameterName;
	private String rutaArchivo;
	private String nombreArchivo;
	private Date fechaEjecucion;

	public void setFileParameterName(String fileParameterName) {
		this.fileParameterName = fileParameterName;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public UUID getIdJob() {
		return UUID.randomUUID();
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	@Transformer
	public JobLaunchRequest toRequest(Message<File> message) {
		LOGGER.info("[Start][toRequest]input:[valor={ '{}' }]", message);
		JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
		jobParametersBuilder.addString(fileParameterName, message.getPayload().getAbsolutePath());
		jobParametersBuilder.addString(nombreArchivo, message.getPayload().getName());
		jobParametersBuilder.addString(rutaArchivo, message.getPayload().getPath());
		jobParametersBuilder.addDate("fechaEjecucion", getFechaEjecucion());
		jobParametersBuilder.addString("idJob", getIdJob().toString());
		LOGGER.info("[End][toRequest]Output:[valor={ '{}' }]", jobParametersBuilder);
		return new JobLaunchRequest(job, jobParametersBuilder.toJobParameters());
	}

}
