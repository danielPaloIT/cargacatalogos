package mx.com.cardif.carga.catalogos.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "dbo", name = "PSN_CAT_CALLES")
public class PsnCatCalles {

	@Id
	@Column(name = "NUMEROCALLE")
	private Integer numeroCalle;

	@Column(name = "NOMBRECALLE", columnDefinition = "char")
	private String nombreCalle;

	@Column(name = "ID_BITACORA", columnDefinition = "uniqueidentifier")
	private String idBitacora;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	public PsnCatCalles() {
		// default implementation ignored
	}

	public Integer getNumeroCalle() {
		return numeroCalle;
	}

	public void setNumeroCalle(Integer numeroCalle) {
		this.numeroCalle = numeroCalle;
	}

	public String getNombreCalle() {
		return nombreCalle;
	}

	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}

	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public String toString() {
		return "PsnCatCalles [numeroCalle=" + numeroCalle + ", nombreCalle=" + nombreCalle + ", idBitacora="
				+ idBitacora + ", fechaCreacion=" + fechaCreacion + "]";
	}

}
