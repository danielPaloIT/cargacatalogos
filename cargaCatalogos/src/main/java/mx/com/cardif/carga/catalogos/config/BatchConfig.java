package mx.com.cardif.carga.catalogos.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import mx.com.cardif.carga.catalogos.commons.CatalogosDTO;
import mx.com.cardif.carga.catalogos.commons.SpringConfigConstant;
import mx.com.cardif.carga.catalogos.dao.PsnBitacoraCargaCatalogos;
import mx.com.cardif.carga.catalogos.util.DBWritterBitacoraCargaCatalogos;
import mx.com.cardif.carga.catalogos.util.DBWritterCatalogos;
import mx.com.cardif.carga.catalogos.util.ProcessorBitacoraCargaCatalogos;
import mx.com.cardif.carga.catalogos.util.ProcessorCatalogos;

@Configuration
@EnableBatchProcessing
public class BatchConfig extends DefaultBatchConfigurer {

	private static final Logger LOGGER = LoggerFactory.getLogger(BatchConfig.class);

	@Override
	protected JobRepository createJobRepository() throws Exception {
		MapJobRepositoryFactoryBean factoryBean = new MapJobRepositoryFactoryBean();

		return factoryBean.getObject();
	}

	@Override
	@Autowired
	public void setDataSource(DataSource dataSource) {
		// default implementation ignored
	}

	@Bean
	public Job cargaCatalogosJob(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
			ProcessorCatalogos itemProcessor, DBWritterCatalogos itemWirtter,
			ProcessorBitacoraCargaCatalogos processorBitacora, DBWritterBitacoraCargaCatalogos writterBitacora) {
		LOGGER.info(
				"[Start][cargaCatalogosJob]input:[valor={jobBuilderFactory,stepBuilderFactory,itemProcessor,itemWirtter,processorBitacora,writterBitacora}]");
		return jobBuilderFactory.get(SpringConfigConstant.BATCH_CONFIG_JOB_CARGA_CATALOGOS)
				.incrementer(new RunIdIncrementer())
				.start(guardaInformacionBitacoraStep(stepBuilderFactory, processorBitacora, writterBitacora))
				.next(cargaCatalogosStep(stepBuilderFactory, itemProcessor, itemWirtter)).build();
	}

	@Bean
	protected Step guardaInformacionBitacoraStep(StepBuilderFactory stepBuilderFactory,
			ProcessorBitacoraCargaCatalogos itemProcessor, DBWritterBitacoraCargaCatalogos itemWirtter) {
		LOGGER.info(
				"[Start][guardaInformacionBitacoraStep]input:[valor={jobBuilderFactory,stepBuilderFactory,ProcessorBitacoraCargaCatalogos,DBWritterBitacoraCargaCatalogos}]");
		return stepBuilderFactory.get(SpringConfigConstant.BATCH_CONFIG_STEP_GUARDA_BITACORA)
				.<String, PsnBitacoraCargaCatalogos>chunk(10).reader(itemReader(null)).processor(itemProcessor)
				.writer(itemWirtter).build();
	}

	@Bean
	protected Step cargaCatalogosStep(StepBuilderFactory stepBuilderFactory, ProcessorCatalogos itemProcessor,
			DBWritterCatalogos itemWirtter) {
		LOGGER.info(
				"[Start][cargaCatalogosStep]input:[valor={jobBuilderFactory,stepBuilderFactory,ProcessorCatalogos,DBWritterCatalogos}]");
		return stepBuilderFactory.get(SpringConfigConstant.BATCH_CONFIG_STEP_CARGA_CATALOGOS)
				.<String, CatalogosDTO>chunk(1).reader(itemReader(null)).processor(itemProcessor).writer(itemWirtter)
				.build();
	}

	@Bean
	@StepScope
	public FlatFileItemReader<String> itemReader(@Value("#{jobParameters[input_file_name]}") String archivo) {
		LOGGER.info("[Start][itemReader]input:[valor={ '{}' }]", archivo);
		FlatFileItemReader<String> reader = new FlatFileItemReader<>();
		reader.setResource(new FileSystemResource(archivo));
		reader.setLineMapper(new PassThroughLineMapper());
		LOGGER.info("[End][itemReader]Output:[valor={ '{}' }]", reader);
		return reader;
	}

}
