package mx.com.cardif.carga.catalogos.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.com.cardif.carga.catalogos.commons.CatalogosDTO;
import mx.com.cardif.carga.catalogos.dao.IPsnCatCallesRepo;
import mx.com.cardif.carga.catalogos.dao.IPsnCatZonasRepo;

@Component
public class DBWritterCatalogos implements ItemWriter<CatalogosDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DBWritterCatalogos.class);

	@Autowired
	private IPsnCatCallesRepo calleRepository;

	@Autowired
	private IPsnCatZonasRepo zonaRepository;

	@Override
	public void write(List<? extends CatalogosDTO> catalogos) throws Exception {
		LOGGER.info("[Start][writeCatalogo]input:[valor={ '{}' }]", catalogos);
		for (CatalogosDTO catalogo : catalogos) {
			if (catalogo.getCalle() != null) {
				LOGGER.debug("[writeCatalogoCalle]");
				calleRepository.save(catalogo.getCalle());
			} else if (catalogo.getZona() != null) {
				LOGGER.debug("[writeCatalogoZona]");
				zonaRepository.save(catalogo.getZona());
			}
		}
	}

}
