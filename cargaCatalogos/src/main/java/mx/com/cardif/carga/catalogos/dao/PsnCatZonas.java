package mx.com.cardif.carga.catalogos.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "dbo", name = "PSN_CAT_ZONAS")
public class PsnCatZonas {

	@Id
	@Column(name = "numerociudad")
	private Integer numeroCiudad;

	@Column(name = "numerocolonia")
	private Integer numeroColonia;

	@Column(name = "nombrezona", columnDefinition = "char")
	private String nombreZona;

	@Column(name = "poblacionzona", columnDefinition = "char")
	private String poblacionZona;

	@Column(name = "municipiozona", columnDefinition = "char")
	private String municipioZona;

	@Column(name = "codigopostalzona")
	private Integer codigoPostalZona;

	@Column(name = "planozona", columnDefinition = "char")
	private String planoZona;

	@Column(name = "rumbozona", columnDefinition = "char")
	private String rumboZona;

	@Column(name = "supervisorzona")
	private Integer supervisorZona;

	@Column(name = "choferzona")
	private Integer choferZona;

	@Column(name = "jefegrupozona")
	private Integer jefeGrupoZona;

	@Column(name = "gerentezona")
	private Integer gerenteZona;

	@Column(name = "abogadozona")
	private Integer abogadoZona;

	@Column(name = "marcaencuesta30dias", columnDefinition = "char")
	private String marcaEncuestaDias;

	@Column(name = "numerocalle")
	private Integer numeroCalle;

	@Column(name = "numerocasa")
	private Integer numeroCasa;

	@Column(name = "marcaunidadhabitacional", columnDefinition = "char")
	private String marcaUnidadHabitacional;

	@Column(name = "numerodivisioncobranzas")
	private Integer numeroDivisionCobranzas;

	@Column(name = "claveabogado")
	private Integer claveAbogado;

	@Column(name = "ciudadcobranzas")
	private Integer ciudadCobranzas;

	@Column(name = "numerocobranzas")
	private Integer numeroCobranzas;

	@Column(name = "clavearagon", columnDefinition = "char")
	private String claveAragon;

	@Column(name = "centro")
	private Integer centro;

	@Column(name = "ID_BITACORA", columnDefinition = "uniqueidentifier")
	private String idBitacora;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	public PsnCatZonas() {
		// default implementation ignoredX
	}

	public Integer getNumeroCiudad() {
		return numeroCiudad;
	}

	public void setNumeroCiudad(Integer numeroCiudad) {
		this.numeroCiudad = numeroCiudad;
	}

	public Integer getNumeroColonia() {
		return numeroColonia;
	}

	public void setNumeroColonia(Integer numeroColonia) {
		this.numeroColonia = numeroColonia;
	}

	public String getNombrezona() {
		return nombreZona;
	}

	public void setNombrezona(String nombreZona) {
		this.nombreZona = nombreZona;
	}

	public String getPoblacionZona() {
		return poblacionZona;
	}

	public void setPoblacionZona(String poblacionZona) {
		this.poblacionZona = poblacionZona;
	}

	public String getMunicipioZona() {
		return municipioZona;
	}

	public void setMunicipioZona(String municipioZona) {
		this.municipioZona = municipioZona;
	}

	public Integer getCodigoPostalZona() {
		return codigoPostalZona;
	}

	public void setCodigoPostalZona(Integer codigoPostalZona) {
		this.codigoPostalZona = codigoPostalZona;
	}

	public String getPlanoZona() {
		return planoZona;
	}

	public void setPlanoZona(String planoZona) {
		this.planoZona = planoZona;
	}

	public String getRumboZona() {
		return rumboZona;
	}

	public void setRumboZona(String rumboZona) {
		this.rumboZona = rumboZona;
	}

	public Integer getSupervisorZona() {
		return supervisorZona;
	}

	public void setSupervisorZona(Integer supervisorZona) {
		this.supervisorZona = supervisorZona;
	}

	public Integer getChoferZona() {
		return choferZona;
	}

	public void setChoferZona(Integer choferZona) {
		this.choferZona = choferZona;
	}

	public Integer getJefeGrupoZona() {
		return jefeGrupoZona;
	}

	public void setJefeGrupoZona(Integer jefeGrupoZona) {
		this.jefeGrupoZona = jefeGrupoZona;
	}

	public Integer getGerenteZona() {
		return gerenteZona;
	}

	public void setGerenteZona(Integer gerenteZona) {
		this.gerenteZona = gerenteZona;
	}

	public Integer getAbogadoZona() {
		return abogadoZona;
	}

	public void setAbogadoZona(Integer abogadoZona) {
		this.abogadoZona = abogadoZona;
	}

	public String getMarcaEncuestaDias() {
		return marcaEncuestaDias;
	}

	public void setMarcaEncuestaDias(String marcaEncuestaDias) {
		this.marcaEncuestaDias = marcaEncuestaDias;
	}

	public Integer getNumeroCalle() {
		return numeroCalle;
	}

	public void setNumeroCalle(Integer numeroCalle) {
		this.numeroCalle = numeroCalle;
	}

	public Integer getNumeroCasa() {
		return numeroCasa;
	}

	public void setNumeroCasa(Integer numeroCasa) {
		this.numeroCasa = numeroCasa;
	}

	public String getMarcaUnidadHabitacional() {
		return marcaUnidadHabitacional;
	}

	public void setMarcaUnidadHabitacional(String marcaUnidadHabitacional) {
		this.marcaUnidadHabitacional = marcaUnidadHabitacional;
	}

	public Integer getNumeroDivisionCobranzas() {
		return numeroDivisionCobranzas;
	}

	public void setNumeroDivisionCobranzas(Integer numeroDivisionCobranzas) {
		this.numeroDivisionCobranzas = numeroDivisionCobranzas;
	}

	public Integer getClaveAbogado() {
		return claveAbogado;
	}

	public void setClaveAbogado(Integer claveAbogado) {
		this.claveAbogado = claveAbogado;
	}

	public Integer getCiudadCobranzas() {
		return ciudadCobranzas;
	}

	public void setCiudadCobranzas(Integer ciudadCobranzas) {
		this.ciudadCobranzas = ciudadCobranzas;
	}

	public Integer getNumeroCobranzas() {
		return numeroCobranzas;
	}

	public void setNumeroCobranzas(Integer numeroCobranzas) {
		this.numeroCobranzas = numeroCobranzas;
	}

	public String getClaveAragon() {
		return claveAragon;
	}

	public void setClaveAragon(String claveAragon) {
		this.claveAragon = claveAragon;
	}

	public Integer getCentro() {
		return centro;
	}

	public void setCentro(Integer centro) {
		this.centro = centro;
	}

	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public String toString() {
		return "PsnCatZonas [numeroCiudad=" + numeroCiudad + ", numeroColonia=" + numeroColonia + ", idBitacora="
				+ idBitacora + ", fechaCreacion=" + fechaCreacion + "]";
	}

}
