package mx.com.cardif.carga.catalogos.commons;

public class SpringConfigConstant {
	
	private SpringConfigConstant() {}
	
	public static final String INTEGRATION_CONFIG_ADVICE_BEAN_NAME = "processor";
	public static final String INTEGRATION_CONFIG_ADVICE_METHOD_NAME = "moverArchivoProcesado";
	public static final String INTEGRATION_CONFIG_TRANSFORMER_FILE_PATH = "input_file_name";
	public static final String INTEGRATION_CONFIG_TRANSFORMER_FILE_NAME = "file_name";
	
	public static final String BATCH_CONFIG_JOB_CARGA_CATALOGOS = "cargaCatalogosJob";
	public static final String BATCH_CONFIG_STEP_GUARDA_BITACORA = "guardaBitacoraStep";
	public static final String BATCH_CONFIG_STEP_CARGA_CATALOGOS = "cargaCatalogosStep";
	
	
	public static final String DATASOURCE_CONFIG_TRANSACTION_MANAGER = "catalogosTransactionManager";
	public static final String DATASOURCE_CONFIG_ENTITY_MANAGER = "catalogosEntityManager";
	public static final String DATASOURCE_CONFIG_PERSISTENCE_UNIT = "catalogosPersistence";
	public static final String DATASOURCE_CONFIG_PACKAGE_SCAN = "mx.com.cardif.carga.catalogos";

	
	

}
