package mx.com.cardif.carga.catalogos.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.com.cardif.carga.catalogos.dao.IPsnBitacoraCargaCatalogosRepo;
import mx.com.cardif.carga.catalogos.dao.PsnBitacoraCargaCatalogos;

@Component
public class DBWritterBitacoraCargaCatalogos implements ItemWriter<PsnBitacoraCargaCatalogos> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DBWritterBitacoraCargaCatalogos.class);

	@Autowired
	private IPsnBitacoraCargaCatalogosRepo bitacoraRepository;

	@Override
	public void write(List<? extends PsnBitacoraCargaCatalogos> bitacora) throws Exception {
		LOGGER.info("[Start][writeBitacora]input:[valor={ '{}' }]", bitacora);
		bitacoraRepository.save(bitacora.get(0));
	}

}
