package mx.com.cardif.carga.catalogos.config;

import java.io.File;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.integration.launch.JobLaunchingGateway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.FileHeaders;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;

import mx.com.cardif.carga.catalogos.commons.SpringConfigConstant;
import mx.com.cardif.carga.catalogos.util.ArhivoProcesado;
import mx.com.cardif.carga.catalogos.util.FileMessageToJobRequest;

@Configuration
public class IntegrationConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationConfig.class);

	@Value("${carga.catalogos.directorio.fuente}")
	private String directorioOrigen;

	@Value("${carga.catalogos.directorio.procesados}")
	private String directorioDestino;

	@Value("${carga.catalogos.extension.archivos}")
	private String extensionArchivo;

	@Value("${carga.catalogos.fixed.delay:1000}")
	private int fixedDelay;

	protected DirectChannel inputChannel() {
		return new DirectChannel();
	}

	@Bean
	public IntegrationFlow integrationFlow(JobLaunchingGateway jobLaunchingGateway, Job job,
			JobRepository jobRepository) {
		LOGGER.info("[Start][integrationFlow]input:[valor={jobLaunchingGateway,jobRepository}]");
		return IntegrationFlows
				.from(Files.inboundAdapter(new File(directorioOrigen))
						.filter(new SimplePatternFileListFilter(extensionArchivo)),
						c -> c.poller(Pollers.fixedRate(fixedDelay).maxMessagesPerPoll(1)))
				.channel(inputChannel()).transform(fileMessageToJobRequest(job))
				.handle(jobLaunchingGateway(jobRepository))
				.handle(SpringConfigConstant.INTEGRATION_CONFIG_ADVICE_BEAN_NAME,
						SpringConfigConstant.INTEGRATION_CONFIG_ADVICE_METHOD_NAME, e -> e.advice(advice()))
				.get();
	}

	@Bean
	protected FileMessageToJobRequest fileMessageToJobRequest(Job job) {
		FileMessageToJobRequest transformer = new FileMessageToJobRequest();
		transformer.setJob(job);
		transformer.setFileParameterName(SpringConfigConstant.INTEGRATION_CONFIG_TRANSFORMER_FILE_PATH);
		transformer.setNombreArchivo(SpringConfigConstant.INTEGRATION_CONFIG_TRANSFORMER_FILE_NAME);
		transformer.setRutaArchivo(SpringConfigConstant.INTEGRATION_CONFIG_TRANSFORMER_FILE_PATH);
		transformer.setFechaEjecucion(new Date());
		return transformer;
	}

	@Bean
	protected JobLaunchingGateway jobLaunchingGateway(JobRepository jobRepository) {
		SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
		simpleJobLauncher.setJobRepository(jobRepository);
		simpleJobLauncher.setTaskExecutor(new SyncTaskExecutor());
		return new JobLaunchingGateway(simpleJobLauncher);
	}

	@Bean
	protected AbstractRequestHandlerAdvice advice() {
		return new AbstractRequestHandlerAdvice() {
			@Override
			protected Object doInvoke(ExecutionCallback callback, Object target, Message<?> message) {
				LOGGER.info("[Start][advice]");
				File file = message.getHeaders().get(FileHeaders.ORIGINAL_FILE, File.class);
				try {
					Object result = callback.execute();
					Boolean response = file.renameTo(new File(directorioDestino, file.getName()));
					LOGGER.info("[End][advice][Archivo se movio a procesado correctamente] {}", response);
					return result;
				} catch (Exception e) {
					LOGGER.error("[Error][Ocurrio un error al mover el archivo]: {}", e.getMessage());
					return null;
				}
			}
		};
	}

	@Bean
	protected ArhivoProcesado processor() {
		return new ArhivoProcesado();
	}

}
